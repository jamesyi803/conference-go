import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": city + " " + str(state),
        "per_page": 1
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        return {
            "weather": ((content["main"]["temp"]-273.15) * 9 / 5) + 32,
            "description": content["weather"][0]["description"]
            }
    except:
        return {"weather": None}
